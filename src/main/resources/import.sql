INSERT INTO regiones (id,nombre) VALUES (1,'Sudamérica');
INSERT INTO regiones (id,nombre) VALUES (2,'Centroaérica');
INSERT INTO regiones (id,nombre) VALUES (3,'Norteamérica');
INSERT INTO regiones (id,nombre) VALUES (4,'Europa');
INSERT INTO regiones (id,nombre) VALUES (5,'Africa');
INSERT INTO regiones (id,nombre) VALUES (6,'Oceanía');
INSERT INTO regiones (id,nombre) VALUES (7,'Sudamérica');
INSERT INTO regiones (id,nombre) VALUES (8,'Antártida');

INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi1','Condori1','prueba@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi2','Condori2','prueba2@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi3','Condori3','prueba3@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi4','Condori4','prueba4@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi5','Condori5','prueba5@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi6','Condori6','prueba6@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi7','Condori7','prueba87@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi8','Condori8','prueba8@dirislimasur.gob.pe','2018-01-01');
INSERT INTO cliente (region_id,nombre,apellido,email,create_at) values(1,'Yosshi9','Condori9','prueba34@dirislimasur.gob.pe','2018-01-01');


INSERT INTO usuarios (username,password,enabled,nombre,apellido,email) values ('yosshi','$2a$10$cZZgPadxUFcZVtXzT/.zbeBVx9f0TL3o7smZQhq4GBsVYbulPLj9O',1,'Yosshi','Condori','yosshi@gmail.com');
INSERT INTO usuarios (username,password,enabled,nombre,apellido,email) values ('admin','$2a$10$cZZgPadxUFcZVtXzT/.zbeBVx9f0TL3o7smZQhq4GBsVYbulPLj9O',1,'Salvador','Mendieta','salvador@gmail.com');

INSERT INTO roles (nombre) values ('ROLE_USER');
INSERT INTO roles (nombre) values ('ROLE_ADMIN');

INSERT INTO usuarios_roles (usuario_id,role_id) values(1,1);
INSERT INTO usuarios_roles (usuario_id,role_id) values(2,2);
INSERT INTO usuarios_roles (usuario_id,role_id) values(2,1);

INSERT INTO productos(nombre,precio,create_at) VALUES('Lg',259990,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Sony Camara',123490,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Apple Ipod',1499990,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Sony Notebook Z110',37990,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Hewlett Packard',69990,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Bianchi',69990,NOW());
INSERT INTO productos(nombre,precio,create_at) VALUES('Mica Comoda',29990,NOW());

INSERT INTO facturas (descripcion,observacion,cliente_id,create_at) VALUES ('Factura equipos de oficina',null,1,NOW());

INSERT INTO facturas_items(cantidad,factura_id,producto_id) VALUES(1,1,1);
INSERT INTO facturas_items(cantidad,factura_id,producto_id) VALUES(2,1,4);
INSERT INTO facturas_items(cantidad,factura_id,producto_id) VALUES(1,1,5);
INSERT INTO facturas_items(cantidad,factura_id,producto_id) VALUES(1,1,7);

INSERT INTO facturas (descripcion,observacion,cliente_id,create_at) VALUES('Factura Bicicleta','Alguna nota importante!',1,NOW());
INSERT INTO facturas_items(cantidad,factura_id,producto_id) VALUES(3,2,6);



