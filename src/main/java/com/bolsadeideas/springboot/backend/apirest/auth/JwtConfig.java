package com.bolsadeideas.springboot.backend.apirest.auth;

public class JwtConfig {
	
	public static final String LLAVE_SECRETA= "alguna.clave.secreta.12345678";
	
	public static final String RSA_PRIVATE="-----BEGIN RSA PRIVATE KEY-----\r\n"
			+ "MIIEpAIBAAKCAQEA1qt2blW/O38rn3UtyJ2buMugTJvPz9mCA6NQdCBD2DjJx7Te\r\n"
			+ "5vmolKa1n/YSNPTb1AJriO/v7x3XHBYEXpMPSjQ2IFGVmv3EodWf+NYcZ6ugVMkg\r\n"
			+ "voAJ/z55NPqAxNmCnRKMGT3pyOGfgZOJs48Lt2OegLg3D0HF2u/D9tlYRNsWsjv1\r\n"
			+ "9TkFrUjQvnF3HenSBna5CvpdHWzldEraMbDJnKzocGxqA7qUMnDTb9ezyTrDiBV7\r\n"
			+ "WZwDxbuAwfd/qNP3KeQlGgqUS7npqHAVuEGUwLxQ5cr3Z953TPWyqTJTFm0sT06s\r\n"
			+ "wFAD8Dq6+uOK+QPmwcPildqoe9sOvIMZWB277wIDAQABAoIBAA98nzamiP1J12PR\r\n"
			+ "mFP4ytsyFYZxVzSvhGmuxocaLMrUjfoSWmJfcIII5OF9ZyLa52EMK6XdaXnetT4Z\r\n"
			+ "eIc6JMg9rAn42MottJnYR4o0KChygUacm/giVJZNc4EW8WNCLeIXDHRluw/69v7q\r\n"
			+ "He0OTNIWJcEziQVYwWViFskgNPYCllriqBknc+3qt7YZmNY1stt09ovcF0gSXAFi\r\n"
			+ "zibmm/qL8uFrLnXoHbS1jKDEfBT6XV0kY8/DzLy+grj6UT7IYnEP5/uBkoCwH6+z\r\n"
			+ "VwGM86ALRxoae4/LfivI6oLv36LuHWolb0lltS1v6umZAH9/0gyoolWhPQPB8h2o\r\n"
			+ "0MmobgECgYEA/YhxE9kti9BRUMxvDomOWkLd6GK0GiKNBXonPW72BTdqJU1EJ69a\r\n"
			+ "fZ4F5sO5YNjZC8wwsOMGj0Lg5LUV3qlNHpC+/dhWl5SmHigclp4/mGWcPNg8UiRy\r\n"
			+ "yY4+730O12j+93KeqKf0O9JND3kO3X9u3aOGu5IIQCtYF7UweY5J/+ECgYEA2MI2\r\n"
			+ "JXoQ1PhUU0kBKqXDXpP1Z1qYZIqYnYANHOKGoa4JSm1VUJCph8639I94KNfGhKW1\r\n"
			+ "F1dqwuJoSHoy8YHSJn1Lpf0t0Gj3L5Ja7ij0RwXZ87UbHf4qYYwVSM++2YWzfnxe\r\n"
			+ "78y3sI5Gcx0f70nqt9+3ythP4cNbOp+jRtsWdc8CgYEAo1h1AkS6g4oD2frc0pDt\r\n"
			+ "llnefwqY3v3DvfOltH75IwNA8CeH8cmYk7ZsNvGDTlJoBKHZ/R1H7KU6ICBLptmN\r\n"
			+ "RlQbuZDYuXCcVnGheakbcCSHNOxccM/q5NEw2zvHhCGxo9vFPQR45tHntEBaw3qH\r\n"
			+ "A2WIOPEtP6qtZxjl0mtKYgECgYEAoQdO0HKpdH3zr1fJ7RPuWyHSKvELy42HKWLK\r\n"
			+ "2w2SdSRktI0a11pRB5pT4PSAkN8UFNFCAlAn+RXX4RgfJnmKVJu6ilrXIUAJeqbg\r\n"
			+ "tJNqPiUjsMB3n8WFWKiH22qyXsjL+wiMQtu0Dw7zWWtmoQ8pt2YVFTWzyoWmDZww\r\n"
			+ "4V+7hNMCgYBEyptMwz7mZ+zIPfCP+ON2b2Vq2S0LTSfzpaZYVrL7xMs2+5MQKktY\r\n"
			+ "UhA1U4byvcn1XQN5rIGgUdFJ8Z4m1F1ypbWbKTQjPKsMWgRplGk+baQsZYE9Exkk\r\n"
			+ "lHmPZWfFKqxMhrcGctqmkHgmOnsjgnfb4lMH+/RAh9TBAeXkAlQC3w==\r\n"
			+ "-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLIC="-----BEGIN PUBLIC KEY-----\r\n"
			+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1qt2blW/O38rn3UtyJ2b\r\n"
			+ "uMugTJvPz9mCA6NQdCBD2DjJx7Te5vmolKa1n/YSNPTb1AJriO/v7x3XHBYEXpMP\r\n"
			+ "SjQ2IFGVmv3EodWf+NYcZ6ugVMkgvoAJ/z55NPqAxNmCnRKMGT3pyOGfgZOJs48L\r\n"
			+ "t2OegLg3D0HF2u/D9tlYRNsWsjv19TkFrUjQvnF3HenSBna5CvpdHWzldEraMbDJ\r\n"
			+ "nKzocGxqA7qUMnDTb9ezyTrDiBV7WZwDxbuAwfd/qNP3KeQlGgqUS7npqHAVuEGU\r\n"
			+ "wLxQ5cr3Z953TPWyqTJTFm0sT06swFAD8Dq6+uOK+QPmwcPildqoe9sOvIMZWB27\r\n"
			+ "7wIDAQAB\r\n"
			+ "-----END PUBLIC KEY-----";

}
