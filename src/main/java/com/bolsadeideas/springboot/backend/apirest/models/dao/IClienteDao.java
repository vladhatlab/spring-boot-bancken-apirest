package com.bolsadeideas.springboot.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Cliente;
import com.bolsadeideas.springboot.backend.apirest.models.entity.Region;

//Extends para Retornar Metodos Crud Desde la Entitidad Cliente y el primer valor de la tabla
//en este caso es Long

public interface IClienteDao extends JpaRepository<Cliente,Long>{
		
		//Trabajamos con Objetos, no con tablas
		//llamamos a la clase
		@Query("from Region")
		public List<Region> findAllRegiones();
	
}
