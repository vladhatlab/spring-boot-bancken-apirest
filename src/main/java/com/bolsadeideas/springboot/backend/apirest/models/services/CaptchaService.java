package com.bolsadeideas.springboot.backend.apirest.models.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.devglan.userportal.RecaptchaResponse;

@PropertySource("classpath:application.properties")
@Service
public class CaptchaService {

	private final RestTemplate restTemplate;
	
	public CaptchaService(RestTemplateBuilder  restTemplateBuiler) {
		this.restTemplate=restTemplateBuiler.build();
	}
	@Value("${google.recaptcha.secret.key}")
	public String recaptchaSecret;
	
	@Value("{google.recaptcha.verify.url}")
	public String recaptchaVerifyUrl;
	
	public boolean verify(String response) {
		MultiValueMap param= new LinkedMultiValueMap<>();
		param.add("secret",recaptchaSecret);
		param.add("response",response);
		
		RecaptchaResponse recaptchaResponse = null;
	}
	
}
